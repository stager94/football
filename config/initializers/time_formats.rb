Date::DATE_FORMATS[:original_date] = ->(date) { 
	if Date.today == date
		I18n.t 'today'
	elsif Date.yesterday == date
		I18n.t 'yesterday'
	else
		I18n.l date, format: "%A, %-d %B %Y", locale: :ru
	end
}