class Player < ActiveRecord::Base
	after_create :resave
	before_update :set_photos_uri

  belongs_to :club
  has_and_belongs_to_many :article
  has_and_belongs_to_many :position
  attr_accessible :birthday, :country, :height, :name, :nickname, :number, :photo, :position, :photos_uri, :weight, :club_id, :biography, :position_ids
  serialize :photos_uri, Hash

	has_attached_file :photo, styles: {small: "50x50#", medium: "200x300>", large: "340x540>"},
														storage: :dropbox,
														dropbox_credentials: Rails.root.join("config/dropbox.yml"),
														path: "football/:rails_env/players/:style/:id.:extension"

	def age
		now = Time.now.utc.to_date
		now.year - birthday.year - (birthday.to_date.change(:year => now.year) > now ? 1 : 0)
	end

	private
	def resave
		self.save!
	end

	def set_photos_uri
		self.photos_uri = {small: self.photo.url(:small), medium: self.photo.url(:medium), large: self.photo.url(:large)}
	end
end
