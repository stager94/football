class CategoryNew < ActiveRecord::Base
	has_many :articles, foreign_key: :category_id

	acts_as_nested_set
	attr_accessible :display, :title, :parent_id
	attr_protected :lft, :rgt
	belongs_to :parent, class_name: 'CategoryNew'

	scope :roots, where(parent_id: nil)

	def full_path
		arr = Array.new
		# self_and_ancestors.each do |a|
		# 	arr << a.title
		# end
		# return arr.join('.')
		arr << parent.title if parent_id != nil
		arr << title
		return arr.join('.')
	end

	def siblings
		CategoryNew.where(parent_id: id)
	end

	def child?
		parent_id != nil
	end
end