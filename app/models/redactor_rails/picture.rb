class RedactorRails::Picture < RedactorRails::Asset
  has_attached_file :data,
  									storage: :dropbox,
										dropbox_credentials: Rails.root.join("config/dropbox.yml"),
										dropbox_options: {environment: ENV["RACK_ENV"]},
										path: "football/:rails_env/articles/:id/:style/:basename.:extension",                  
                    styles: { content: '800>', thumb: '118x100#' }

  validates_attachment_size :data, less_than: 2.megabytes
  validates_attachment_presence :data

  def url_content
    url(:content)
  end
end