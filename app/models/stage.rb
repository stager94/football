class Stage < ActiveRecord::Base
  belongs_to :competition
  has_many :matches
  attr_accessible :title, :competition_id

	def self.group_competition
		result = Hash.new
		@stages = Stage.all.group_by {|stage| stage.competition.title}
		@stages.each do |competition, stages|
			result[competition] = Array.new
			stages.map do |stage|
				result[competition] << [stage.title, stage.id]
			end
		end
		result
	end
end
