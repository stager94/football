class Article < ActiveRecord::Base
	paginates_per 5
	# belongs_to :category_new
	belongs_to :category, class_name: 'CategoryNew'
	has_and_belongs_to_many :club
	has_and_belongs_to_many :player

  before_update :set_image_uri
  after_create :resave
	serialize :images_uri, Hash
	acts_as_taggable
  attr_accessible :display, :image, :snippet, :text, :title, :youtube, :category_new_id, :tag_list, :category_id, 
  								:club_ids, :player_ids, :author, :image_author
	has_attached_file :image, styles: {small: "390x214#", thumb: "749x431#"},
														storage: :dropbox,
														dropbox_credentials: Rails.root.join("config/dropbox.yml"),
														path: "football/:rails_env/news/:style/:id.:extension"

	SIDES = %w(left right)

	def resave
		self.save!
	end

	def set_image_uri
		self.images_uri = {small: self.image.url(:small), thumb: self.image.url(:thumb), original: self.image.url(:original)}
	end
end
