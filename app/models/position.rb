class Position < ActiveRecord::Base
	has_and_belongs_to_many :player

  attr_accessible :name, :alias, :short, :line, :description
  
  validates_presence_of :name, :alias, :short, :line

  POSITION_LABEL = %w(warning primary success danger)
end
