class Competition < ActiveRecord::Base
  before_update :set_logo_uri
  after_create :resave

	attr_accessible :logo, :snippet, :title, :logos_uri, :country
	has_many :matches
	has_many :clubs
	serialize :logos_uri, Hash
	validates_presence_of :title, :logo
	has_attached_file :logo, styles: {thumb: "100x100>", small: "50x50>"},
													storage: :dropbox,
													dropbox_credentials: Rails.root.join("config/dropbox.yml"),
													path: "football/:rails_env/competitions/:style/:id.:extension"

	def logo_url(style = :thumb)
		# logo.url("#{style}") if logo.exists?("#{style}")
		self.logos_uri[:"#{style}"]
	end

  def resave
    self.save!
  end

  def set_logo_uri
    self.logos_uri = {small: self.logo.url(:small), thumb: self.logo.url(:thumb), original: self.logo.url(:original)}
  end

end
