class Category < ActiveRecord::Base
  belongs_to :category
  has_many :articles
  attr_accessible :display, :feed, :title, :category_id
end
