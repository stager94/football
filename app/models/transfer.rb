class Transfer < ActiveRecord::Base
  belongs_to :player
  belongs_to :from, class_name: 'Club'
  belongs_to :to, class_name: 'Club'
  attr_accessible :amount, :salary, :salary_per, :salary_unit, :unit, :player_id, :from_id, :to_id, :date

  UNITS = [
  	['U.S. dollar', :usd],
  	['Euro', :eur],
  	['Ukrainian hryvnia', :uah],
  	['Pound sterling', :gbp]
  ]

  PER = [
  	['Weekly', :w],
  	['Monthly', :m],
  	['1 Year', :y]
  ]
end
