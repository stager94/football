class Club < ActiveRecord::Base
	acts_as_gmappable
	before_update :set_logo_uri

	after_create :resave
	
	has_many :matches
	belongs_to :competition
	has_and_belongs_to_many :article

	serialize :logos_uri, Hash
	serialize :information, Hash

	attr_accessible :alias, :emblem, :snippet, :title, :country, :competition_id, :information, :city, :address, :latitude, :longitude
	geocoded_by :address
	after_validation :geocode, if: :address_changed?
	validates_presence_of :alias, :emblem, :snippet, :title

	has_attached_file :emblem, styles: {thumb: "100x100>", small: "50x50>", big: "250x250>", gmaps: "32x32>"},
														storage: :dropbox,
														dropbox_credentials: Rails.root.join("config/dropbox.yml"),
														path: "football/:rails_env/clubs/:style/:id.:extension"

	INFORMATION = %w(full_name names year stadium capacity president coach capitain rating www)

	def self.group_competition
		result = Hash.new
		@clubs = Club.all.group_by {|club| club.competition.try(:title)}
		@clubs.each do |competition, clubs|	
			result[competition] = Array.new
			clubs.map do |club|
				result[competition] << [club.title, club.id]
			end
		end
		result
	end

	def logo_url(style = :thumb)
		self.logos_uri[:"#{style}"]
		# logo.url(:"#{style}")
	end

	def logo
		emblem
	end

	def resave
		self.save!
	end

	def set_logo_uri
		self.logos_uri = {small: self.logo.url(:small), thumb: self.logo.url(:thumb), original: self.logo.url(:original), big: self.logo.url(:big), gmaps: self.logo.url(:gmaps)}
	end

	def gmaps4rails_address
 		"#{information['stadium']}, #{city}, #{country}"
	end
end
