class Channel < ActiveRecord::Base
	serialize :logos_uri, Hash
	
  before_update :set_logo_uri
  before_create :set_logo_uri

	has_and_belongs_to_many :matches
	attr_accessible :logo, :title, :url

	validates_presence_of :title, :logo
	has_attached_file :logo, styles: {thumb: "100x100>", small: "50x50>"},
													storage: :dropbox,
													dropbox_credentials: Rails.root.join("config/dropbox.yml"),
													path: "football/:rails_env/channels/:style/:id.:extension"

	def logo_url(style = :thumb)
		self.logos_uri[:"#{style}"] unless logos_uri.nil?
	end


  def set_logo_uri
    self.logos_uri = {small: self.logo.url(:small), thumb: self.logo.url(:thumb), original: self.logo.url(:original)}
  end
end