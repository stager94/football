class Match < ActiveRecord::Base
  before_create :set_date_end
  before_update :set_date_end

  has_and_belongs_to_many :channel
  belongs_to :home, class_name: 'Club'
  belongs_to :guest, class_name: 'Club'
  belongs_to :competition
  belongs_to :stage
  attr_accessible :date, :snippet, :channel_ids, :home_id, :guest_id, :competition_id, :stage_id

  scope :today, where("(date >= '#{Time.now.beginning_of_day}' and date <= '#{Time.now.end_of_day}') OR (date > '#{Time.now.beginning_of_day-1.hour}')").order('date ASC')
  scope :this_week, where(date: DateTime.now.utc.beginning_of_week..DateTime.now.utc.end_of_week).order('date ASC')

  def state_class
  	if self.over?
  		'over'
  	elsif self.start?
  		'live'
  	else
  		''
  	end
  end

  def start?
  	range = self.date..self.date+1.hour+45.minute
  	range.cover?(Time.now.utc+3.hour)
  end

  def over?
  	(Time.now.utc+3.hour)>self.date+1.hour+45.minute
  end

  def channels
  	channel_titles = Array.new
  	self.channel.each do |channel|
  		channel_titles << channel.title
  	end
  	channel_titles.join(', ')
  end

  def set_date_end
    self.date_end = self.date+1.hour+45.minute
  end

  def competition_title
    "#{competition.title}.#{stage.try(:title)}"
  end
end
