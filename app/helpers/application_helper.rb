module ApplicationHelper
	def sidebar_matches_module
		@matches = Match.includes(:channel, :competition, :home, :guest).today.group_by(&:competition_title)
		
		render 'shared/modules/sidebar/matches'
	end

	def related_materials
		if %w(articles).include?(controller_name) && %w(show).include?(action_name)
			@article = Article.find(params[:id])
			@articles = @article.find_related_tags.order('created_at DESC').group_by {|article| "#{distance_of_time_in_words(article.created_at, Time.now)}" }

			render 'shared/modules/sidebar/related'
		end
	end

	def categories
		if %w(articles category_news).include?(controller_name) && %w(feed show).include?(action_name)
			# @category = CategoryNew.find(params[:category_id])
			@categories = CategoryNew.roots.includes(:parent)
			render 'shared/modules/sidebar/categories'
		end
	end

	def club_info
		if %w(clubs).include?(controller_name) && %w(show).include?(action_name)
			@club = Club.find(params[:id])
			render 'shared/modules/sidebar/club_info'
		end
	end

	def club_map
		if %w(clubs).include?(controller_name) && %w(show).include?(action_name)
			@club = Club.find(params[:id])
			unless @club.address
				@json = @club.to_gmaps4rails do |club, marker|
					marker.title "#{@club.title}"
					marker.json({population: @club.information[:capacity]})
					marker.picture({picture: "#{@club.logos_uri[:small]}", width: 50, height: 50})
				end
				render 'shared/modules/sidebar/club_map'
			end
		end
	end

	def simple_format(text)
		text.gsub(/\\n/, '<br />') || ''
	end

	def clubs_near
		if %w(clubs).include?(controller_name) && %w(show).include?(action_name)
			@club = Club.find(params[:id])
			@clubs = @club.nearbys(210, units: :km)

			render 'shared/modules/sidebar/clubs_near'
		end
	end

	def player_info
		if %w(players).include?(controller_name) && %w(show).include?(action_name)
			@player = Player.includes(:position, :club).find(params[:id])
			render 'shared/modules/sidebar/player_info'
		end
	end

	def articles_clubs
		if %w(articles).include?(controller_name) && %w(show).include?(action_name)
			@article = Article.find(params[:id])
			@clubs = @article.club
			@players = @article.player

			render 'shared/modules/sidebar/articles_clubs'
		end
	end

	def last_transfers
		@transfers = Transfer.limit(5).order(:created_at)

		render 'shared/modules/sidebar/last_transfers'
	end
end
