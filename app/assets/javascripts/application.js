// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery.role
//= require bootstrap
//= require fixHead
//= require tipTip
//= require_tree .

$(document).ready(function(){
	$('.nav-table a').click(function(){
		var content = $(this).parent('td').parent('tr').parent('tbody').parent('table').attr('data-content');
		var id = $(this).attr('href');

		$('.nav-table td').removeClass('active');
		$(this).parent('td').addClass('active');
		$('.tab-pane', content).addClass('hide');
		$('.tab-pane'+id, content).removeClass('hide');

		var	height = $('.tab-pane'+id, content).innerHeight();
		console.log(height);
		$('#tv-content').css('height', height+20+'px');

		return false;
	});

	$('.root-category a.root').click(function(){
		$(this).toggleClass('active');
		$('small', this).toggleClass('hide');
		var $id = $(this).attr("data-id");
		var $child_list = $('.child-list[data-parent=' + $id + ']');
		var $child_list_link = $('.child-list[data-parent=' + $id + '] a');

		var $innerHeight = $child_list_link.innerHeight();
		var $length = $child_list_link.length;
		var $height = $innerHeight*$length;

		var $child_list_height = $child_list.css('height');
		if ($child_list_height == '1px') {
			$child_list.css('height', $height+'px');
		} else {
			$child_list.css('height', '1px');
		}
		$('.modules').trigger('changeHeight');
		return false;
	});


	$('#tv-content').css('height', $('#tv-content').innerHeight() + 'px');

	$('.news-chooser').Chooser();
	$('.modules').fixHead();
	$('.tipTip').tipTip({defaultPosition: 'top'});
});