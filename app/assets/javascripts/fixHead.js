(function( $ ) {
	$.fn.fixHead = function() {
		var self = this;
		var padding = 50;
		$(window).load(function(){
			// Get the necessary heights
			var blockHeight  = self.height();
			var windowHeight = $(window).height() - padding;
			// Get the started scroll params
			var scrollTop      = $(window).scrollTop();
			var cacheScrollTop = scrollTop;
			// Tail
			var tail = blockHeight - windowHeight;
			// Get the top and bottom coordinates of the block
			var posYTop      = self.offset().top;
			var posYBottom   = posYTop + blockHeight;
			var posYTopFixed = scrollTop - tail;
			// Switchers
			var stickBottom = false;
			var stickTop    = false;
			var leftHeight = $('.col-lg-8').height();

			// LOGGING
			console.log('Window Height:' + windowHeight);
			console.log('Block Height:' + blockHeight);
			console.log('Tail:' + tail);
			console.log('posYTop: ' + posYTop);

			// On scrolling the page
			$(window).scroll(function(){
				self.css('width', '');
				blockWidth = $('.container .row .col-lg-4').width();
				windowWidth = $(window).width();
				windowHeight = $(window).height() - padding;
				blockHeight = self.height();
				tail = blockHeight - windowHeight;
				self.css('width', blockWidth);
				posYTop = self.offset().top;
				posYBottom = posYTop + blockHeight;

				if (windowWidth > 1220) {
					scrollTop = $(window).scrollTop();
					console.log(leftHeight + ' - ' + blockHeight + ' - ' + tail);
					if (leftHeight > blockHeight) {
						console.log('Tail is ' + tail);
						if (tail <= 0) {
							self.css('position', 'fixed');
							self.css('top', 50);
							self.css('bottom', '');
						} else {
							if (scrollTop > cacheScrollTop) {
								if (stickTop == true) {
									posYTop = self.offset().top - padding;
									posYBottom = posYTop + blockHeight;

									self.css('position', 'relative');
									self.css('top', posYTop);
									stickTop = false;
								} else {
									// Down the page
									if (scrollTop + windowHeight > posYBottom) { 
										self.css('position', 'fixed');
										self.css('top', '');
										self.css('bottom', 20);
										stickBottom = true;
									}
								}
							} else {
								if (stickBottom == true) {
									posYTop = self.offset().top - padding;
									posYBottom = posYTop + blockHeight;

									self.css('position', 'relative');
									self.css('top', posYTop);
									stickBottom = false;
								} else {
									if (scrollTop < posYTop) {
										self.css('position', 'fixed');
										self.css('top', 50);
										stickTop = true;
									}
								}

							}
						}
					} else {
						// self.css('position', 'fixed');
						if (blockHeight < windowHeight) {
							self.css('position', 'fixed');
							self.css('top', 50);
							self.css('bottom', '');
							self.css('width', '');
						}
					}
				} else {
					self.css('position', '');
					self.css('top', '');
					self.css('bottom', '');
				}
				
				
				cacheScrollTop = scrollTop;
			});
		});

		$(window).resize(function(){
			windowHeight = $(window).height() - padding;
			self.css('width', '');
			blockWidth = $('.container .row .col-lg-4').width();
			blockHeight = self.height();
			tail = blockHeight - windowHeight;
			self.css('width', blockWidth);
			posYTop = self.offset().top;
			posYBottom = posYTop + blockHeight;
			console.log('New Window Height: ' + windowHeight);
			console.log('New Tail: ' + tail);
		});

		self.bind("changeHeight", function(){
			setTimeout(function(){
				windowHeight = $(window).height() - padding;
			  blockHeight = self.height();
			  tail = blockHeight - windowHeight;
			  console.log(blockHeight + ' - ' + tail);
			}, 2000);
		});
	}
})(jQuery);