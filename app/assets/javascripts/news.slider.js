(function( $ ) {
	$.fn.Chooser = function() {
		var self = this;
		var active_news_content = $('.large-list .news-content', self).first();
		var active_thumb_content = $('.thumb-list img').first();
		var temp_active_thumb_content = null;
		var temp_avtive_news_content = null;
		var thumbs_list = $('.thumb-list img', self);
		var news_list = $('.large-list .news-content', self);
		var hover = false;

		$image = $('img', news_list);
		$image.load(function(){
			var $height = $(this).height();	
			$('.large-list', self).css('height', $height);
		});

		news_list.animate({opacity: 0}, 0);

		thumbs_list.fadeTo(0, .7);
		active_thumb_content.fadeTo(0, 1).addClass('active');
		active_news_content.fadeTo(0, 1);
		active_news_content.css('z-index', '1');

		self.hover(function(){
			hover = true;
		}, function(){
			hover = false;
		});

		thumbs_list.hover(function(){
			$(this).stop().fadeTo('slow', 1);
		}, function(){
			$this = $(this);
			if (!$this.hasClass('active')) $this.stop().fadeTo('slow', .7);
		});

		thumbs_list.click(function(){
			var id = $(this).attr('data-id');
			if (id != active_news_content.attr('id')) {
				temp_avtive_news_content = $('.large-list .news-content#' + id);
				temp_active_thumb_content = $(this);
				self.display();
			}
		});

		self.display = function() {
			active_news_content.animate({opacity: 0}, {queue: false, duration: 'slow'});
			temp_avtive_news_content.animate({opacity: 1}, {queue: false, duration: 'slow'});
			active_news_content.css('z-index', '0');
			temp_avtive_news_content.css('z-index', '1')
			active_news_content = temp_avtive_news_content;

			active_thumb_content.animate({opacity: .7}, 1500).removeClass('active');
			temp_active_thumb_content.fadeTo('slow', 1).addClass('active');
			active_thumb_content = temp_active_thumb_content;
		}

		self.tick = function() {
			if (hover == false) {
				var $next = active_thumb_content.next('img');
				if ($next.length == 0) $next = thumbs_list.first();
				var $id = $next.attr('data-id')
				temp_avtive_news_content = $('.large-list .news-content#' + $id);
				temp_active_thumb_content = $next;
				self.display();
			}
		}
		setInterval(self.tick, 5000);

		$(window).resize(function(){
			var $height = $('.news-content', self).height();
			$('.large-list', self).css('height', $height);
		});
	};
})(jQuery);