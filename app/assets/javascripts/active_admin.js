//= require active_admin/base
//= require redactor-rails
//= require select2
$(document).ready(function(){
	$('.select2').select2();
	$('.select2-tags').select2({tags: [], tokenSeparators: [","]});
});