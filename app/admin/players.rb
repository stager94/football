ActiveAdmin.register Player do
	form do |f|
		f.inputs "General" do
			f.input :name
			f.input :nickname
			f.input :birthday, as: :date_picker
			f.input :height
			f.input :weight
			f.input :position, input_html: {class: "select2", style: 'width: 500px;'}, collection: Position.order('line ASC').each {|p| [p.name, p.id]}
			f.input :country, priority_countries: %w(UA RU GB IT ES BR), input_html: {class: "select2", style: 'width: 500px;'}
			f.input :club, input_html: {class: "select2", style: 'width: 500px;'}
			f.input :number
			f.input :photo, as: :file
		end

		f.inputs "Biography" do
			f.input :biography, input_html: {class: "redactor"}, label: false
		end
		f.buttons
	end 

	index do
		selectable_column
		id_column
		column :photo do |player|
			image_tag player.photos_uri[:medium]
		end
		column :name
		column :birthday
		column :country
		column :club
		column :number
		default_actions
	end 
end
