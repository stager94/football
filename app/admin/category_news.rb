ActiveAdmin.register CategoryNew do
	index do
		selectable_column
		id_column
		column :title
		column :parent_id do |category|
			link_to "#{category.parent.try(:title)}", admin_category_news_path(category.parent) if category.child?
		end
		column :display
		column :feed
		default_actions
	end

	form do |f|
		f.inputs "General Information" do
			f.input :title
			f.input :parent_id, as: :select, collection: nested_set_options(CategoryNew) {|i| "#{'-' * i.level} #{i.title}" }, input_html: {class: "select2", style: 'width: 500px;'}
			f.input :display
			f.input :feed
		end
		f.buttons
	end  
end
