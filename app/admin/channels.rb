ActiveAdmin.register Channel do
	index do
		selectable_column
		id_column
		column :logo do |channel|
			raw "#{image_tag channel.logo_url(:small)}"
		end
		column :title
		column :url
		default_actions
	end

	form do |f|
		f.inputs "General Information" do
			f.input :title
			f.input :url
			f.input :logo, as: :file
		end
		f.buttons
	end
end
