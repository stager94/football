ActiveAdmin.register Competition do
	index do
		selectable_column
		id_column
		column :logo do |competition|
			raw "#{image_tag competition.logo_url(:small)}"
		end
		column :title
		column :snippet
		column :country
		default_actions
	end

	form do |f|
		f.inputs "General Information" do
			f.input :title
			f.input :snippet
			# f.input :logo, as: :file
  		if f.object.logo.exists?(:thumb)
  			f.input :logo, as: :file, hint: f.template.image_tag(f.object.logo.url(:thumb)) 
  		else
  			f.input :logo, as: :file
  		end
			f.input :country, priority_countries: %w(EU W), input_html: {class: "select2", style: 'width: 500px;'}		
		end
		f.buttons
	end
end
