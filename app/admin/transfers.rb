ActiveAdmin.register Transfer do
	index do
		selectable_column
		id_column
		column :date
		column :player
		column :from
		column :to
		column :amount do |player|
			"#{number_to_human player.amount} #{I18n.t player.unit, scope: 'money.names'}"
		end
		column :salary do |player|
			"#{number_to_human player.salary} #{I18n.t player.salary_unit, scope: 'money.names'} / #{I18n.t player.salary_per, scope: 'salary.per'}"
		end
		default_actions
	end

  form do |f|
  	f.inputs "General Information" do
  		f.input :player, input_html: {class: "select2", style: 'width: 500px;'}
  		f.input :from, input_html: {class: "select2", style: 'width: 500px;'}, collection: Club.group_competition
  		f.input :to, input_html: {class: "select2", style: 'width: 500px;'}, collection: Club.group_competition
  		f.input :amount
  		f.input :unit, as: :select, collection: Transfer::UNITS, input_html: {class: "select2", style: 'width: 500px;'}
  		f.input :salary
  		f.input :salary_per, as: :select, collection: Transfer::PER, input_html: {class: "select2", style: 'width: 500px;'}
  		f.input :salary_unit, as: :select, collection: Transfer::UNITS, input_html: {class: "select2", style: 'width: 500px;'}
  		f.input :date, as: :datepicker
  	end
  	f.buttons
  end
end
