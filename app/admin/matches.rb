ActiveAdmin.register Match do
  index do
  	selectable_column
  	id_column
  	column :channel do |match|
      match.channels
    end
  	column :home
  	column :guest
  	column :date
  	column :competition
  	default_actions
  end

  form do |f|
  	f.inputs "General Information" do
  		f.input :channel, input_html: {class: "select2", style: 'width: 500px;'}
  		f.input :date, as: :datetime_picker
  		f.input :home, input_html: {class: "select2", style: 'width: 500px;'}, collection: Club.group_competition
  		f.input :guest, input_html: {class: "select2", style: 'width: 500px;'}, collection: Club.group_competition
  		f.input :competition, input_html: {class: "select2", style: 'width: 500px;'}
      f.input :stage, input_html: {class: "select2", style: 'width: 500px;'}, collection: Stage.group_competition
  		f.input :snippet, as: :text
  	end
  	f.buttons
  end
end