ActiveAdmin.register Position do
	form partial: 'form'

	index do
		selectable_column
		column :short do |position|
			raw "<span class='status_tag complete'>#{position.short}</span>"
		end
		column :line
		column :name
		column :alias
		column :description do |position|
			raw truncate position.description
		end
		default_actions
	end
end
