ActiveAdmin.register Article do
	index do
		selectable_column
		id_column
		column :title
		column :category do |article|
      link_to article.category.full_path, admin_category_news_path(article.category)
    end
		column :tag_list
		column :display
		default_actions
	end

  form do |f|
  	f.inputs "General Information" do
  		f.input :title
      f.input :author
  		f.input :category, as: :select, collection: nested_set_options(CategoryNew) {|i| "#{'-' * i.level} #{i.title}" }, input_html: {class: "select2", style: 'width: 500px;'}
      f.input :image, as: :file
      f.input :image_author
  		f.input :youtube
  		f.input :tag_list, input_html: {class: "select2-tags", style: 'width: 500px;'}
      f.input :club, input_html: {class: "select2", style: 'width: 500px;'}
      f.input :player, input_html: {class: "select2", style: 'width: 500px;'}
  		f.input :snippet
  		f.input :text, input_html: {class: "redactor"}, label: false
  		f.input :display
  	end
  	f.buttons
  end

  show do
  end
end
