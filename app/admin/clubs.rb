ActiveAdmin.register Club do
	index do
		selectable_column
    column :emblem do |club|
      raw "#{image_tag club.logo_url}"
    end
		column :title
		column :alias
    column :competition
    column :country
    column :city
    # column :address
    # column :latitude
    # column :longitude
    column :information do |club|
      raw "<div><b>Full name:</b> #{club.information[:full_name]}</div>
        <div><b>Nicknames:</b> #{club.information[:names]}</div>
        <div><b>Foundation Date:</b> #{club.information[:year]}</div>
        <div><b>Stadium:</b> #{club.information[:stadium]}</div>
        <div><b>Capacity:</b> #{club.information[:capacity]}</div>
        <div><b>President:</b> #{club.information[:president]}</div>
        <div><b>Coach:</b> #{club.information[:coach]}</div>
        <div><b>Captain:</b> #{club.information[:capitain]}</div>
        <div><b>Rating:</b> #{club.information[:rating]}</div>
        <div><b>Site:</b> #{club.information[:www]}</div>"
    end
		default_actions
	end

  form partial: 'form'

end
