class ArticlesController < ApplicationController
	def feed
		@articles_arr = Article.includes(taggings: :tag, tags: :taggings, category: :parent).order('created_at DESC').page(params[:page])
		@articles = @articles_arr.group_by {|article| article.created_at.strftime('%d.%m.%Y') }
	end

	def show
		@article = Article.includes(:category, :tags).find(params[:id])
	end
end