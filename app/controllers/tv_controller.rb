class TvController < ApplicationController
	def index
		@days_arr = (Date.today.beginning_of_week..Date.today.end_of_week).map(&:to_s)
		@matches = Match.includes(:channel, :home, :guest, :competition).this_week.group_by{|m| m.date.strftime("%Y-%m-%d") }
		# binding.pry
	end
end