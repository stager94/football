class HomeController < ApplicationController
  def index
  	@news = Article.order('created_at DESC').limit(3)
  end
end
