class ClubsController < ApplicationController
	respond_to :json, :js, :html

	def index
		@clubs = Club.all
		
		@json = @clubs.to_gmaps4rails do |club, marker|
			marker.infowindow render_to_string(partial: "/clubs/infowindow", locals: { club: club})
			marker.title "#{club.title}"
			marker.json({population: club.information[:capacity]})
			marker.picture({picture: "#{club.logos_uri[:gmaps]}", width: 32, height: 32})
		end
	end

	def show
		@club = Club.find(params[:id])
		
		@json = @club.to_gmaps4rails do |club, marker|
			marker.infowindow render_to_string(partial: "/clubs/infowindow", locals: { club: @club})
			marker.title "#{@club.title}"
			marker.json({population: @club.information[:capacity]})
			marker.picture({picture: "#{@club.logos_uri[:small]}", width: 50, height: 50})
		end

		# render json: Club.find(params[:id])
	end
end
