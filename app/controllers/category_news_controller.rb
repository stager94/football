class CategoryNewsController < ApplicationController
	def show
		@category = CategoryNew.find(params[:id])
		@articles_arr = Article.where(category_id: @category.id).includes(taggings: :tag, tags: :taggings, category: :parent).order('created_at DESC').page(params[:page])
		@articles = @articles_arr.group_by {|article| article.created_at.strftime('%d.%m.%Y') }
	end
end
