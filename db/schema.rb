# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130826093731) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "articles", :force => true do |t|
    t.string   "title"
    t.text     "snippet"
    t.text     "text"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "youtube"
    t.boolean  "display"
    t.string   "tags"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.text     "images_uri"
    t.integer  "category_id"
    t.integer  "club_id"
    t.integer  "player_id"
    t.string   "author"
    t.string   "image_author"
  end

  add_index "articles", ["category_id"], :name => "index_news_on_category_id"
  add_index "articles", ["club_id"], :name => "index_articles_on_club_id"
  add_index "articles", ["player_id"], :name => "index_articles_on_player_id"

  create_table "articles_clubs", :id => false, :force => true do |t|
    t.integer "club_id"
    t.integer "article_id"
  end

  create_table "articles_players", :id => false, :force => true do |t|
    t.integer "player_id"
    t.integer "article_id"
  end

  create_table "categories", :force => true do |t|
    t.string   "title"
    t.integer  "category_id"
    t.boolean  "feed",        :default => false
    t.boolean  "display",     :default => true
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
  end

  add_index "categories", ["category_id"], :name => "index_categories_on_category_id"

  create_table "category_news", :force => true do |t|
    t.string   "title"
    t.boolean  "display",    :default => true
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "lft"
    t.integer  "depth"
    t.integer  "parent_id"
    t.integer  "rgt"
    t.boolean  "feed",       :default => false
  end

  create_table "channels", :force => true do |t|
    t.string   "title"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "url"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.text     "logos_uri"
  end

  create_table "channels_matches", :id => false, :force => true do |t|
    t.integer "channel_id"
    t.integer "match_id"
  end

  create_table "clubs", :force => true do |t|
    t.string   "title"
    t.string   "alias"
    t.text     "snippet"
    t.string   "emblem_file_name"
    t.string   "emblem_content_type"
    t.integer  "emblem_file_size"
    t.datetime "emblem_updated_at"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.text     "logos_uri"
    t.integer  "competition_id"
    t.string   "country"
    t.text     "information"
    t.string   "city"
    t.string   "address"
    t.float    "longitude"
    t.float    "latitude"
    t.boolean  "gmaps"
  end

  add_index "clubs", ["competition_id"], :name => "index_clubs_on_competition_id"

  create_table "clubs_articles", :id => false, :force => true do |t|
    t.integer "club_id"
    t.integer "article_id"
  end

  create_table "competitions", :force => true do |t|
    t.string   "title"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.text     "snippet"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.text     "logos_uri"
    t.string   "country"
  end

  create_table "matches", :force => true do |t|
    t.integer  "channel_id"
    t.datetime "date"
    t.integer  "home_id"
    t.integer  "guest_id"
    t.integer  "competition_id"
    t.text     "snippet"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.datetime "date_end"
    t.integer  "stage_id"
  end

  add_index "matches", ["channel_id"], :name => "index_matches_on_channel_id"
  add_index "matches", ["competition_id"], :name => "index_matches_on_competition_id"
  add_index "matches", ["guest_id"], :name => "index_matches_on_guest_id"
  add_index "matches", ["home_id"], :name => "index_matches_on_home_id"
  add_index "matches", ["stage_id"], :name => "index_matches_on_stage_id"

  create_table "players", :force => true do |t|
    t.string   "name"
    t.string   "nickname"
    t.date     "birthday"
    t.string   "country"
    t.integer  "height"
    t.integer  "weight"
    t.string   "position"
    t.integer  "club_id"
    t.integer  "number"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.text     "photos_uri"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.text     "biography"
    t.integer  "position_id"
  end

  add_index "players", ["club_id"], :name => "index_players_on_club_id"
  add_index "players", ["position_id"], :name => "index_players_on_position_id"

  create_table "players_articles", :id => false, :force => true do |t|
    t.integer "player_id"
    t.integer "article_id"
  end

  create_table "players_positions", :id => false, :force => true do |t|
    t.integer "player_id"
    t.integer "position_id"
  end

  create_table "positions", :force => true do |t|
    t.string   "name"
    t.string   "short"
    t.string   "alias"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "line"
    t.text     "description"
  end

  create_table "redactor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.datetime "data_updated_at"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "redactor_assets", ["assetable_type", "assetable_id"], :name => "idx_redactor_assetable"
  add_index "redactor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_redactor_assetable_type"

  create_table "stages", :force => true do |t|
    t.string   "title"
    t.integer  "competition_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "stages", ["competition_id"], :name => "index_stages_on_competition_id"

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       :limit => 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"
  add_index "taggings", ["taggable_id", "taggable_type", "context"], :name => "index_taggings_on_taggable_id_and_taggable_type_and_context"

  create_table "tags", :force => true do |t|
    t.string "name"
  end

  create_table "transfers", :force => true do |t|
    t.integer  "player_id"
    t.integer  "from_id"
    t.integer  "to_id"
    t.integer  "amount"
    t.string   "unit"
    t.integer  "salary"
    t.string   "salary_per"
    t.string   "salary_unit"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.date     "date"
  end

  add_index "transfers", ["from_id"], :name => "index_transfers_on_from_id"
  add_index "transfers", ["player_id"], :name => "index_transfers_on_player_id"
  add_index "transfers", ["to_id"], :name => "index_transfers_on_to_id"

end
