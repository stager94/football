class CreateCompetitions < ActiveRecord::Migration
  def change
    create_table :competitions do |t|
      t.string :title
      t.attachment :logo
      t.text :snippet

      t.timestamps
    end
  end
end
