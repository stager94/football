class ChangeLgtToRgtInCategoryNew < ActiveRecord::Migration
  def change
  	remove_column :category_news, :lgt
  	add_column :category_news, :rgt, :integer
  end
end
