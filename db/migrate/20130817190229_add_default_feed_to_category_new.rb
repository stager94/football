class AddDefaultFeedToCategoryNew < ActiveRecord::Migration
  def change
  	change_column :category_news, :feed, :boolean, default: false
  end
end
