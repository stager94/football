class AddGmapsToClub < ActiveRecord::Migration
  def change
    add_column :clubs, :gmaps, :boolean
  end
end
