class AddAddressToClub < ActiveRecord::Migration
  def change
    add_column :clubs, :address, :string
    add_column :clubs, :longitude, :float
    add_column :clubs, :latitude, :float
  end
end