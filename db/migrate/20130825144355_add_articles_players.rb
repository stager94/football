class AddArticlesPlayers < ActiveRecord::Migration
	def change
		create_table :articles_players, id: false do |t|
			t.integer :player_id
			t.integer :article_id
		end
	end
end
