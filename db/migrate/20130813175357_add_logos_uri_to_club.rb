class AddLogosUriToClub < ActiveRecord::Migration
  def change
    add_column :clubs, :logos_uri, :text
  end
end
