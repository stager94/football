class CreateTransfers < ActiveRecord::Migration
  def change
    create_table :transfers do |t|
      t.references :player
      t.references :from
      t.references :to
      t.integer :amount
      t.string :unit
      t.integer :salary
      t.string :salary_per
      t.string :salary_unit

      t.timestamps
    end
    add_index :transfers, :player_id
    add_index :transfers, :from_id
    add_index :transfers, :to_id
  end
end
