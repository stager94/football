class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :title
      t.references :category
      t.boolean :feed, default: false
      t.boolean :display, default: true

      t.timestamps
    end
    add_index :categories, :category_id
  end
end
