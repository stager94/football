class AddPlayersArticles < ActiveRecord::Migration
	def change
		create_table :players_articles, id: false do |t|
			t.integer :player_id
			t.integer :article_id
		end
	end
end
