class AddClubIdToArticle < ActiveRecord::Migration
  def change
    add_column :articles, :club_id, :integer
    add_column :articles, :player_id, :integer
    add_index :articles, :club_id
    add_index :articles, :player_id
  end
end
