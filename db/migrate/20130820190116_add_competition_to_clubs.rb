class AddCompetitionToClubs < ActiveRecord::Migration
  def change
    add_column :clubs, :competition_id, :integer
    add_column :clubs, :country, :string
    add_index :clubs, :competition_id
  end
end
