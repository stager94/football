class AddLogosUriToChannel < ActiveRecord::Migration
  def change
    add_column :channels, :logos_uri, :text
  end
end
