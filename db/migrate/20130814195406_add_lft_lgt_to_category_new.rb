class AddLftLgtToCategoryNew < ActiveRecord::Migration
  def change
  	remove_column :category_news, :parent_id
    add_column :category_news, :lft, :integer
    add_column :category_news, :lgt, :integer
    add_column :category_news, :depth, :integer
    add_column :category_news, :parent_id, :integer
  end
end
