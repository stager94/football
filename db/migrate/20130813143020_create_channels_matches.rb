class CreateChannelsMatches < ActiveRecord::Migration
	def change
		create_table :channels_matches, id: false do |t|
			t.integer :channel_id
			t.integer :match_id
		end
	end
end
