class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.string :title
      t.attachment :logo
      t.string :url

      t.timestamps
    end
  end
end
