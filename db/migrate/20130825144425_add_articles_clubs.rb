class AddArticlesClubs < ActiveRecord::Migration
	def change
		create_table :articles_clubs, id: false do |t|
			t.integer :club_id
			t.integer :article_id
		end
	end
end
