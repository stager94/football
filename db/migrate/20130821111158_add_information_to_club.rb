class AddInformationToClub < ActiveRecord::Migration
  def change
    add_column :clubs, :information, :text
  end
end