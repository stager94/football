class CreateStages < ActiveRecord::Migration
  def change
    create_table :stages do |t|
      t.string :title
      t.references :competition

      t.timestamps
    end
    add_index :stages, :competition_id
  end
end
