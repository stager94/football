class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.references :category_new
      t.text :snippet
      t.text :text
      t.attachment :image
      t.string :youtube
      t.boolean :display
      t.string :tags

      t.timestamps
    end
    add_index :news, :category_new_id
  end
end
