class AddAuthorToArticle < ActiveRecord::Migration
  def change
    add_column :articles, :author, :string
    add_column :articles, :image_author, :string
  end
end
