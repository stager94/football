class AddPositionIdToPlayer < ActiveRecord::Migration
  def change
    add_column :players, :position_id, :integer
    add_index :players, :position_id
  end
end
