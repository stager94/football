class AddLogosUriToCompetition < ActiveRecord::Migration
  def change
    add_column :competitions, :logos_uri, :text
  end
end
