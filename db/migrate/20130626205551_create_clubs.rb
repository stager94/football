class CreateClubs < ActiveRecord::Migration
  def change
    create_table :clubs do |t|
      t.string :title
      t.string :alias
      t.text :snippet
      t.attachment :emblem

      t.timestamps
    end
  end
end
