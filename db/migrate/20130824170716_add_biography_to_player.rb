class AddBiographyToPlayer < ActiveRecord::Migration
  def change
    add_column :players, :biography, :text
  end
end
