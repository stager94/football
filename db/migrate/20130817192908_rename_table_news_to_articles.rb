class RenameTableNewsToArticles < ActiveRecord::Migration
  def up
  	drop_table :articles
  	rename_table :news, :articles
  end

  def down
  end
end
