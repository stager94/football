class AddEndDateToMatch < ActiveRecord::Migration
  def change
    add_column :matches, :date_end, :datetime
  end
end
