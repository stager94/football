class AddClubsArticles < ActiveRecord::Migration
	def change
		create_table :clubs_articles, id: false do |t|
			t.integer :club_id
			t.integer :article_id
		end
	end
end
