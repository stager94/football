class AddLineToPosition < ActiveRecord::Migration
  def change
    add_column :positions, :line, :integer
  end
end
