class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :name
      t.string :nickname
      t.date :birthday
      t.string :country
      t.integer :height
      t.integer :weight
      t.string :position
      t.references :club
      t.integer :number
      t.attachment :photo
      t.text :photos_uri

      t.timestamps
    end
    add_index :players, :club_id
  end
end
