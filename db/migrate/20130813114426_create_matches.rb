class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.references :channel
      t.datetime :date
      t.references :home
      t.references :guest
      t.references :competition
      t.text :snippet

      t.timestamps
    end
    add_index :matches, :channel_id
    add_index :matches, :home_id
    add_index :matches, :guest_id
    add_index :matches, :competition_id
  end
end
