class CreateCategoryNews < ActiveRecord::Migration
  def change
    create_table :category_news do |t|
      t.string :title
      t.references :parent
      t.boolean :display, default: true

      t.timestamps
    end
    add_index :category_news, :parent_id
  end
end
